﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Unit))]
public class UnitPosition : MonoBehaviour
{
    [SerializeField]
    private int X;

    [SerializeField]
    private int Y;

    public bool HasMovedThisTurn = false;

    public ScriptableEvent OnTurnEndedEvent;

    public int MoveRange = 5;

    void Start()
    {
        X = (int)transform.position.x;
        Y = (int)transform.position.z;
    }

    private void OnEnable()
    {
        OnTurnEndedEvent.EventTriggered += OnTurnEnded;
    }

    private void OnDisable()
    {
        OnTurnEndedEvent.EventTriggered -= OnTurnEnded;
    }

    public void Move(int newX, int newY)
    {
        Debug.Log($"Moving {gameObject.name}");
        var position = transform.position;
        position.x = newX;
        position.z = newY;
        transform.position = position;
        X = newX;
        Y = newY;
        HasMovedThisTurn = true;
    }

    public int Distance(int targetX, int targetY)
    {
        return Mathf.Abs(targetX - X) + Mathf.Abs(targetY - Y);
    }

    public bool IsInRange(int targetX, int targetY)
    {
        return Distance(targetX, targetY) <= MoveRange;
    }


    private void OnTurnEnded()
    {
        HasMovedThisTurn = false;
    }
}
