﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class UnitSelector : MonoBehaviour
{
    [SerializeField]
    private bool IsMouseOver = false;

    [SerializeField]
    private Unit unit;

    [SerializeField]
    private UnitManager unitManager;

    private void OnValidate()
    {
        Assert.IsNotNull(unit, "Unit cant be null in UnitSelector");
        Assert.IsNotNull(unitManager, "UnitManager cant be null in UnitSelector");
    }

    private void OnMouseEnter()
    {
        IsMouseOver = true;
    }

    private void OnMouseExit()
    {
        IsMouseOver = false;
    }

    private void Update()
    {
        if (IsMouseOver && Input.GetButtonDown("Fire1"))
        {
            unitManager.UnitClicked(unit);
        }
    }
}
