﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Unit))]
public class UnitPlayer : MonoBehaviour
{
    public int PlayerIndex;

    public Color Player0Color;
    public Color Player1Color;

    public MeshRenderer Visual;

    private void Start()
    {
        if(PlayerIndex == 0 && Visual != null)
        {
            Visual.material.color = Player0Color;
        }
        else if (PlayerIndex == 1 && Visual != null)
        {
            Visual.material.color = Player1Color;
        }

    }
}
