﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour
{
    public List<Unit> Units;

    public Unit SelectedUnit;

    public ScriptableEvent OnTurnEnded;

    [Tooltip("The index of the player whose turn it is")]
    public int playerTurnIndex = 0;

    public int playerCount = 2;

    public void UnitClicked(Unit unit)
    {
        //TODO
        Debug.Log($"Unit clicked {unit.gameObject.name}");
        if (SelectedUnit == null && unit.Player.PlayerIndex == playerTurnIndex)
        {
            SelectedUnit = unit;
        }
        else
        {
            SelectedUnit = null;
        }
    }

    public void GroundClicked(int X, int Y)
    {
        //TODO
        Debug.Log(string.Format("Ground clicked {0} {1}", X, Y));
        if(SelectedUnit != null)
        {
            if(SelectedUnit.Position.IsInRange(X, Y) && !SelectedUnit.Position.HasMovedThisTurn)
            {
                SelectedUnit.Position.Move(X, Y);
                EndTurn();
            }
        }
    }

    public void EndTurn()
    {
        playerTurnIndex++;
        if (playerTurnIndex >= playerCount)
        {
            playerTurnIndex = 0;
            
        }
        SelectedUnit = null;
        Debug.Log($"Turn ended. Starting turn of player {playerTurnIndex}");
        OnTurnEnded.Trigger();
    }
}
