﻿
using UnityEngine;
[CreateAssetMenu]
public class ScriptableEvent : ScriptableObject
{
    public delegate void OnEventTriggered();

    public event OnEventTriggered EventTriggered;

    public void Trigger()
    {
        EventTriggered.Invoke();
    }
}
