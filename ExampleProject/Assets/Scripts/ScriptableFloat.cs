﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Float")]
public class ScriptableFloat : ScriptableObject
{
    public float Value;
}
