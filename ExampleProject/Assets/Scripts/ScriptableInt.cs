﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Int")]
public class ScriptableInt : ScriptableObject
{
    public int Value;
}
