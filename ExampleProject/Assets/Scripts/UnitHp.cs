﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(Unit))]
public class UnitHp : MonoBehaviour
{
    [SerializeField]
    private ScriptableInt maxHp;

    [SerializeField]
    private int currentHp;

    [SerializeField]
    private Unit unit;

    private void OnValidate()
    {
        Assert.IsNotNull(maxHp, "MaxHp cant be null in UnitHp");
    }

        private void Start()
    {
        currentHp = maxHp.Value;
    }

    public void DealDamage(int amount)
    {
        CurrentHp -= amount;
    }

    public int CurrentHp
    {
        get
        {
            return currentHp;
        }
        private set
        {
            currentHp = value;

            if (currentHp < 0)
            {
                currentHp = 0;
            }

            if (currentHp == 0)
            {
                Die();
            }
        }
    }

    private void Die()
    {
        //TODO
        unit.Remove();
    }
}
