﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Unit : MonoBehaviour
{
    public UnitPosition Position;
    public UnitHp Hp;
    public UnitPlayer Player;

    [SerializeField]
    private UnitManager unitManager;

    private void OnValidate()
    {
        Assert.IsNotNull(Position, "Position cant be null in Unit");
        Assert.IsNotNull(Hp, "Hp cant be null in Unit");
        Assert.IsNotNull(Player, "Player cant be null in Unit");
        Assert.IsNotNull(unitManager, "UnitManager cant be null in Unit");
    }

    void Start()
    {
        unitManager.Units.Add(this);
    }

    void Update()
    {
        
    }

    public void Remove()
    {
        unitManager.Units.Remove(this);
        gameObject.SetActive(false);
    }
}
