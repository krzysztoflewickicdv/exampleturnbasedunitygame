﻿using UnityEngine;
using UnityEngine.Assertions;

public class GroundSelector : MonoBehaviour
{
    private const string GROUND_TAG = "Ground";

    [SerializeField]
    private UnitManager unitManager;

    private void OnValidate()
    {
        Assert.IsNotNull(unitManager, "UnitManager cant be null in Unit");
    }

    void Update()
    {
        if(Input.GetButtonUp("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(ray, out hit))
            {
                if(hit.transform.CompareTag(GROUND_TAG))
                {
                    int hitX = (int)hit.point.x;
                    int hitY = (int)hit.point.z;
                    unitManager.GroundClicked(hitX, hitY);
                }
            }
        }
    }
}
